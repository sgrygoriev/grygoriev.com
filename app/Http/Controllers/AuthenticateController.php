<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Http\Requests;
use Hash;
use JWTAuth;
//use Twilio;
use Aloha\Twilio\Support\Laravel\Facade as Twilio;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\User;
use App\UsersWhitelist;
use App\UserClient;

class AuthenticateController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['requestActivation', 'activate','refresh', 'register','authenticate']]);
       // $this->middleware('jwt.refresh', ['except' => ['register','authenticate']]);
    }

    public function sms(Request $request)
    {
        try {
            //$twilio = new \Aloha\Twilio\Twilio();//'ACef8b0c3b24742ad2d1cf2c27b1057ac6', '9c49c4d20c982152678ae88996c1c363', '+13472189599');
            //$twilio = new \Aloha\Twilio\Twilio();
            //dd($twilio);
            //$sdk = $twilio->getTwilio();
            //$twilio->message('+380509077992', 'Pink Elephants and Happy Rainbows');
            return response()->json(['success' => true]);
        } catch (\Services_Twilio_RestException $e) {
            return response()->json(['success' => false, 'error' => 'gateway_error'], 500);
        }
    }

    public function modifyUserAuth(Request $request, $id, $action)
    {
        $user = null;
        $accountRecord = User::where('id', '=', $id)->firstOrFail();
        //Validate action
        if(!in_array($action, array('activate', 'deactivate', 'approve', 'disapprove'), true)) {
            return response()->json(['success' => false, 'error' => 'invalid_request'], 400);
        }
        //Validate token
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json([
                'success' => false,
                'error' => 'token_invalid',
                'message' => $e->getMessage()], $e->getStatusCode());
        }
        //Validate permissions

        if (isset($user)
            &&  isset($user->group_id)          //Current user exists and is valid
            &&  $user->group_id == 0            //Current user is admin
            && $accountRecord->group_id != 0) { //Target user is not admin account
            switch($action) {
                case 'activate':
                    $accountRecord->activated_at = date("Y-m-d H:i:s");
                    break;
                case 'approve':
                    $accountRecord->approved_at = date("Y-m-d H:i:s");
                    break;
                case 'deactivate':
                    $accountRecord->activated_at = null;
                    break;
                case 'disapprove':
                    $accountRecord->approved_at = null;
                    break;
            }
            $accountRecord->save();
            return response()->json(['success' => true, 'error' => null]);
        }
        return response()->json(['success' => false, 'error' => 'permission_denied'], 401);
    }

    public function requestActivation(Request $request)
    {
        $user = null;
        $input = $request->only( 'udid');

        $validator = Validator::make($input, [
            'udid' => 'required|max:64',
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false, 'error' => 'invalid_request'], 400);
        }

        if(isset($input['udid'])) {
            $user = User::where('udid', '=', $input['udid'])->firstOrFail();
        } else {
            return response()->json(['success' => false, 'error' => 'invalid_request'], 400);
        }

        if (isset($user)) {
            try {
                if($user->activation_code_created_at > date("Y-m-d H:i:s", time() - 60 * 5)) {
                    // Activation sent less than 5 minutes ago
                    return response()->json(['success' => false, 'error' => 'activation_code_sent'], 500);
                } else {
                    // Generate activation code with new time stamp
                    $activationCode = mt_rand(100000, 999999);
                    $user->activation_code = $activationCode;
                    $user->activation_code_created_at = date("Y-m-d H:i:s");
                    $user->save();
                    $result = Twilio::message($user->phone, 'Activation code :' . $activationCode);

                    if(isset($result->error_code)) {
                        return response()->json(['success' => false,
                            'error' => 'gateway_error',
                            'message' => $result->error_code . ' : ' . $result->error_message ], 500);
                    } else {
                        return response()->json(['success' => true, 'sid' => $result->sid]);
                    }
                }
            } catch (\Services_Twilio_RestException $e) {
                return response()->json(['success' => false, 'error' => 'activation_unavailable'], 500);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }


    public function activate(Request $request)
    {
        $input = $request->only('udid', 'code');
        $validator = Validator::make($input, [
            'udid' => 'required|max:64',
            'code' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json(['success'=>false, 'error' => 'invalid_request'], 400);
        }

        $user = null;
        if(isset($input['udid']) && isset($input['code'])) {
            $user = User::where('udid', '=', $input['udid'])->firstOrFail();
        } else {
            return response()->json(['success' => false, 'error' => 'Invalid request'], 400);
        }

        if (!isset($user)) {
            return response()->json(['success' => false, 'error' => 'user_not_found'], 404);
        } else {
            if($user->activation_code_created_at < date("Y-m-d H:i:s", time() - 60 * 5)) {
                // Activation sent more than 5 minutes ago
                return response()->json(['success' => false, 'error' => 'activation_code_expired'], 500);
            } else {
                if($input['code'] ==  $user->activation_code) {//449504
                    $userSecret = str_random(60);
                    $user->password =  Hash::make($userSecret);
                    $user->activated_at = date("Y-m-d H:i:s");
                    $whiteListed = UsersWhitelist::where('phone', '=', $user->phone)->first();
                    if(isset($whiteListed)) {
                        $user->approved_at = date("Y-m-d H:i:s");
                    }
                    $user->save();
                    if(\Config::get('settings.notifications.activation')) {
                        $data = array(
                            'id' => $user->id,
                            'phone' => $user->phone
                        );
                        \Mail::send('auth.emails.activation', $data, function($message) {
                            $message->from(\Config::get('settings.email.notifications.address'), \Config::get('settings.email.notifications.name'));
                            $message->to(\Config::get('settings.email.admin.address'), \Config::get('settings.email.admin.name'));
                            $message->subject('Contact Form');
                        });
                    }
                    return response()->json(['success' => true, 'secret' => $userSecret ]);
                } else {
                    return response()->json(['success' => false, 'error' => 'invalid_code'], 500);
                }
            }
        }
        //get code compare
    }

    public function refresh(Request $request)
    {
        //$response = $next($request);
        // Retrieve all the users in the database and return them

        $token = JWTAuth::getToken();
        try {
            $newToken = JWTAuth::refresh($token);
            return response()->json(['success' => true, 'token' => $newToken])
                ->header('Authorization', 'Bearer '.$newToken);
        } catch (TokenExpiredException $e) {
            return response()->json([
                'success' => false,
                'error' => 'token_invalid',
                'message' => $e->getMessage()], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json([
                'success' => false,
                'error' => 'token_invalid',
                'message' => $e->getMessage()], $e->getStatusCode());
        }
        return response()->json(['success' => false, 'message' => 'token_refresh_failed'], 500);
    }

    public function index()
    {
        // Retrieve all the users in the database and return them
        $users = User::all();
        return $users;
    }

    public function authenticate(Request $request)
    {
       // $credentials = $request->only('email', 'password');
        $credentials = $request->only('udid', 'password');
        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['success' => false, 'error' => 'invalid_credentials'], 401);
            } else {
                if (! $user = JWTAuth::toUser($token)) {
                    return response()->json(['success' => false, 'error' => 'user_not_found'], 404);
                } else {
                    if (! isset($user->activated_at)) {
                        return response()->json(['success' => false, 'error' => 'user_not_activated'], 401);
                    }
                    if (!isset($user->approved_at)) {
                        return response()->json(['success' => false, 'error' => 'user_not_approved'], 401);
                    }
                }
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['success' => false, 'error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
        $userInput = $request->only('phone', 'name', 'email', 'udid', 'os', 'nosms');

        $validationRules = [
            'udid' => 'required|max:64',
            'phone' => 'required|regex:/^(\+)[0-9]{1,2}[0-9]{10}$/',
            'email' => 'email|max:255',
            'os' => 'in:ios,android,web,other'
        ];

        if(isset($userInput['nosms'])) {
            $validationRules['email'] = 'required|email|max:255';
            $validationRules['phone'] = 'regex:/^(\+)[0-9]{1,2}[0-9]{10}$/';
        };

        $validator = Validator::make($userInput, $validationRules);

        if($validator->fails()){
            return response()->json(['success'=>false, 'error' => 'invalid_request'], 400);
        }

        switch ($userInput['os']) {
            case 'ios':
                $userInput['os'] = 1;
                break;
            case 'android':
                $userInput['os'] = 2;
                break;
            default:
                $userInput['os'] = 0;//web
        }

        try {
            // $user = User::where($userInput)->first();
            $user = User::where('udid', '=', $userInput['udid'])->first();
            if(isset($user)) {
                $user->phone = $userInput['phone'];
                $user->os = $userInput['os'];
                $user->name = $userInput['name'] ?: $user->name;
                $user->email = $userInput['email'] ?: $user->email;
                $user->save();
            } else {
                $userInput['udid'] = $userInput['udid'] ?: str_random(60);//if udid not set - generate one
                $user = User::create($userInput);
            }
        } catch (\Illuminate\Database\QueryException $e) {
           $message = isset($e->errorInfo[2]) ?  $e->errorInfo[2] : "user_create_failed";
           return response()->json(['success'=>false, 'message' => $message], 500);
        }
        return response()->json($user);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['success' => false, 'error' => 'user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
           /* try {
                $newToken = $this->auth->setRequest($request)->parseToken()->refresh();
            } catch (TokenExpiredException $e) {
                return response()->json(['token_expired'], $e->getStatusCode());
            } catch (JWTException $e) {
                return response()->json(['token_expired'], $e->getStatusCode());
            }
            // send the refreshed token back to the client
            response()->headers->set('Authorization', 'Bearer '.$newToken);*/
            return response()->json(['success' => false, 'error' => 'token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['success' => false, 'error' => 'token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['success' => false, 'error' => 'token_absent'], $e->getStatusCode());

        }
        // the token is valid and we have found the user via the sub claim
        return response()->json($user);
    }



}
