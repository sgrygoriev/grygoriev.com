<?php

namespace App\Http\Controllers\CLS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CLS\Attachment;

class AttachmentsController extends Controller
{
    public function index()
    {
        $items = Attachment::all();
        return response()->json($items);
    }
}
