<?php

namespace App\Http\Controllers\CLS;

use Illuminate\Http\Request;
use Storage;
use JWTAuth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CLS\Part;
use App\CLS\PartRepair;
use App\CLS\Attachment;

class PartRepairsController extends Controller
{
    public function index() {
        $partRepairs = PartRepair::all();
        return response()->json($partRepairs);
    }

    public function info($code,$id) {
        try {
            $partRecord = Part::where('code','=',$code)->firstOrFail();
            if(isset($partRecord)) {
                $partRepairRecord = PartRepair::where('part_id', '=', $partRecord->id)->where('id','=',$id)->firstOrFail();
                return response()->json($partRepairRecord);
            }
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(array('success' => false, 'message' => 'Not Found'),404);
        }
    }

    public function store(Request $request, $code) {
        //{"client_id":null,"description":"Power Diods Replacement","delivered_at":null,"warranty_expires_at":null}
        $input = $request->all();
        $path = \Config::get('settings.cls.attachments.repairsFilePath');
        try {
            $partRecord = Part::where('code','=',$code)->firstOrFail();
            if(isset($partRecord)) {
                // $partRepairRecord = PartRepair::where('part_id', '=', $partRecord->id);
                try {
                    $input['part_id'] = $partRecord->id;
                    $newPartRepair = PartRepair::create($input);
                    return response()->json($newPartRepair);
                } catch (\Illuminate\Database\QueryException $e) {
                    $message = isset($e->errorInfo[2]) ? $e->errorInfo[2] : "Failed to create";
                    return response()->json(['success' => false, 'message' => $message], 500);
                }
            }
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(array('success' => false, 'message' => 'Not Found'),404);
        }
    }

    public function update(Request $request, $code, $id) {
        //{"id":1,"part_id":"1","client_id":null,"description":"Power Diods Replacement","delivered_at":null,"warranty_expires_at":null,"created_at":"2017-04-07 09:26:49","updated_at":"2017-04-07 09:26:49","deleted_at":null}
        $input = $request->all();
        $path = \Config::get('settings.cls.attachments.repairsFilePath');
        try {
            $partRecord = Part::where('code','=',$code)->firstOrFail();
            if(isset($partRecord)) {
                $partRepairRecord = PartRepair::where('part_id', '=', $partRecord->id)->where('id', '=', $id)->firstOrFail();
                try {
                    $partRepairRecord->description = $input['description'];
                    $partRepairRecord->save();//($input);
                    return response()->json($partRepairRecord);
                } catch (\Illuminate\Database\QueryException $e) {
                    $message = isset($e->errorInfo[2]) ? $e->errorInfo[2] : "Failed to create";
                    return response()->json(['success' => false, 'message' => $message], 500);
                }
            }
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(array('success' => false, 'message' => 'Not Found'),404);
        }
    }

    public function delete(Request $request, $code, $id) {
        try {
            $partRecord = Part::where('code','=',$code)->firstOrFail();
            if(isset($partRecord)) {
                $partRepairRecord = PartRepair::withTrashed()->where('part_id', '=', $partRecord->id)->where('id', '=', $id)->firstOrFail();
                if(isset($partRepairRecord) && !$partRepairRecord->trashed()) {
                    $partRepairRecord->delete();
                    return response()->json(['success' => true, 'message' => 'Deleted', 'id' => $id]);
                } else if($partRepairRecord->trashed()) {
                    return response()->json(['success' => true, 'message' => 'Already deleted', 'id' => $id]);
                }
            }
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(array('success' => false, 'message' => 'Not Found'),404);
        }
    }
}
