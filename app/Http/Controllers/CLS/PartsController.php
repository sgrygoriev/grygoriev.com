<?php

namespace App\Http\Controllers\CLS;


use Illuminate\Http\Request;
use Storage;
use JWTAuth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CLS\Part;
use App\CLS\PartRepair;
use App\CLS\Attachment;

class PartsController extends Controller
{
    public function index()
    {
        $parts = Part::all();
        return response()->json($parts);
    }

    public function info($code)
    {
        $partRecord = Part::where('code', '=', $code)->firstOrFail();
        $partRepairsRecords = PartRepair::where('part_id', '=', $partRecord->id)->get();
        $partRecord['repairs'] = $partRepairsRecords;
        return response()->json($partRecord);
    }

    public function infoPublic($code)
    {
        $partRecord = Part::where('code', '=', $code)->firstOrFail();
       // $partRepairsRecords = PartRepair::where('part_id', '=', $partRecord->id)->get();
       // $partRecord['repairs'] = $partRepairsRecords;
        return response()->json($partRecord);
    }

    public function store(Request $request)
    {

        /* {
        "code":"Z5T40156E016",
        "sn":null,
        "client_id":null,
        "manufacturer_id":null,
        "vendor_id":null,
        "name":"X-NUCLEO-PLC01A1",
        "note":"",
       -- "last_repair_delivered_at":null,
       -- "last_warranty_expires_at":null,
        }
        {"code":"T40156ET500","sn":"000000001","name":"Terminator","note":"Burned out chip", "images":["R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw=="]}
        */
        $input = $request->all();
        $path = \Config::get('settings.cls.attachments.partsFilePath');
        //$input['password'] = Hash::make($input['password']);
        try {
            //TODO validate input - at least code!!! (Code not needed - generate one on the fly)
            $user = JWTAuth::parseToken()->authenticate();
            if(!isset($input['code'])) {
                $input['code'] = 'X' + time();//temporary code
                $part = Part::create($input); //Create new part with the  temporary code
                $partRepair = PartRepair::create(array(
                    'part_id' => $part->id,
                    'repair_code' => $input['repair_code'] ?: '',
                    'created_by' => $user->id,
                    'updated_by' => $user->id
                ));
                //Update with a new code
                $newCode = sprintf("EL%08d",$part->id);
                $part->code = $newCode;
                $part->last_repair_delivered_at = $partRepair->getCreatedAtColumn();
                $part->last_repair_code = $partRepair->repair_code;
                $part->save();
            } else {
                $part = Part::create($input);
                $partRepair = PartRepair::create(array(
                    'part_id' => $part->id,
                    'created_by' => $user->id,
                    'updated_by' => $user->id
                ));
            }

            if (isset($part) && isset($part->id) && isset($input['images']) && count($input['images']) > 0) {
                $token = JWTAuth::getToken();
                $baseName = $path . md5(microtime() . $token);
                foreach ($input['images'] as $index => $image) {
                    $name = $baseName . $index;
                    Storage::put( $name, base64_decode($image['image']), 'public');
                    $ext = '';
                    $mimetype = Storage::getMimetype($name);
                    if (isset(explode('/', $mimetype)[1])) {
                        $ext = explode('/', $mimetype)[1];
                        Storage::rename($name, $name . '.' . $ext);
                    }
                    Attachment::create(
                        array(
                            'entity_type' => 0,//0 part, 1 repair, 2 diag diagram, 3 diag csv
                            'entity_id' => $part->id,
                            'url' => $name . '.' . $ext,
                            'description' => isset($image['description']) ? $image['description'] : ''
                        )
                    );
                }
            }
            return response()->json($part);

        } catch (\Illuminate\Database\QueryException $e) {
            $message = isset($e->errorInfo[2]) ? $e->errorInfo[2] : "Failed to create";
            return response()->json(['success' => false, 'message' => $message], 500);
        }
        return response()->json(['success' => true]);
    }

    public function update(Request $request, $code) {
        //{"id":1,"part_id":"1","client_id":null,"description":"Power Diods Replacement","delivered_at":null,"warranty_expires_at":null,"created_at":"2017-04-07 09:26:49","updated_at":"2017-04-07 09:26:49","deleted_at":null}
        $input = $request->all();
        $path = \Config::get('settings.cls.attachments.partsFilePath');
        try {
            $partRecord = Part::where('code','=',$code)->firstOrFail();
            if(isset($partRecord)) {
                /*
                $t->increments('id')->index();
            $t->string('code')->unique();
            $t->string('sn')->nullable();
            $t->integer('client_id')->index()->nullable();
            $t->integer('manufacturer_id')->index()->nullable();
            $t->integer('vendor_id')->index()->nullable();
            $t->string('name')->default('');//nam
            $t->string('note')->default('');//num
            $t->dateTimeTz('last_repair_delivered_at')->nullable();
            $t->dateTimeTz('last_warranty_expires_at')->nullable();

                 */
                try {
                    $partRecord->sn = $input['sn'];
                    $partRecord->code = $input['code'];
                    $partRecord->client_id = $input['client_id'];
                    $partRecord->manufacturer_id = $input['manufacturer_id'];
                    $partRecord->vendor_id = $input['vendor_id'];
                    $partRecord->name = $input['name'];
                    $partRecord->note = $input['note'];
                    $partRecord->last_repair_delivered_at = $input['last_repair_delivered_at'];
                    $partRecord->last_warranty_expires_at = date('Y-m-d H:i:s', time($partRecord->last_repair_delivered_at));
                    $partRecord->save();//($input);
                    return response()->json($partRecord);
                } catch (\Illuminate\Database\QueryException $e) {
                    $message = isset($e->errorInfo[2]) ? $e->errorInfo[2] : "Failed to create";
                    return response()->json(['success' => false, 'message' => $message], 500);
                }
            }
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(array('success' => false, 'message' => 'Not Found'),404);
        }
    }

    public function delete(Request $request, $code) {
        try {
            $partRecord = Part::withTrashed()->where('code','=',$code)->firstOrFail();
            if(isset($partRecord) && !$partRecord->trashed()) {
                $partRecord->delete();
                return response()->json(['success' => true, 'message' => 'Deleted', 'id' => $partRecord->id]);
            } else if($partRecord->trashed()) {
                return response()->json(['success' => true, 'message' => 'Already deleted', 'id' => $partRecord->id]);
            }
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(array('success' => false, 'message' => 'Not Found'),404);
        }
    }
}
