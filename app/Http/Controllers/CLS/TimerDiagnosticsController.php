<?php

namespace App\Http\Controllers\CLS;

use Illuminate\Http\Request;
use League\Flysystem\Filesystem;

use Storage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use JWTAuth;
use App\User;
use App\CLS\Part;
use App\CLS\TimerDiagnostic;
use App\CLS\Attachment;

class TimerDiagnosticsController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => []]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $diagnostics = TimerDiagnostic::all();
        return response()->json($diagnostics);
    }

    public function info($code) {
        $diagnosticRecords = TimerDiagnostic::where('code','=',$code)->get();
        return $diagnosticRecords;
    }

    public function store(Request $request)
    {
        /*
                    $t->increments('id')->index();
            $t->string('code')->unique();
            $t->string('sn')->nullable();
            $t->integer('client_id')->index()->nullable();
            $t->string('name')->default('');//nam
            $t->string('note')->default('');//num
            $t->dateTimeTz('repair_delivered_at')->nullable();
            $t->dateTimeTz('warranty_expires_at')->nullable();
            $t->timestampsTz();//created_at updated_at (nullable)
            $t->softDeletes();//deleted_at (nullable)



        {
        "code":"Z5T40156E016",
        "sn":null,
        "client_id":null,
        "name":"X-NUCLEO-PLC01A1",
        "note":""
        }
        */
        $input = $request->all();
        //Check if the part exists
        $partRecord = Part::where('code', '=', $input['code'])->first();
        if(!isset($partRecord)) {
            $partFields = array('code' => $input['code'], 'note' => 'Auto-created');
            try {
                $partRecord = Part::create($partFields);
            } catch (\Illuminate\Database\QueryException $e) {
                $message = isset($e->errorInfo[2]) ? $e->errorInfo[2] : "Failed to create";
                return response()->json(['success' => false, 'message' => $message], 500);
            }
        }
        //$attachments = Attachment::all();
        //If it does not exists create the part TODO
        $input['part_id'] = $partRecord->id;
        $path = \Config::get('settings.cls.attachments.timerDiagnosticsFilePath');//'public/cls/timers';
        try {
            $diagnosticRecord = TimerDiagnostic::create($input);
            if (isset($diagnosticRecord) && isset($diagnosticRecord->id)) {
                $token = JWTAuth::getToken();
                $name = $path . md5(microtime() . $token);
                if(isset($input['diagram'])) {
                    Storage::put( $name, base64_decode($input['diagram']), 'public');
                    $ext = '';
                    $mimetype = Storage::getMimetype($name);
                    if (isset(explode('/', $mimetype)[1])) {
                        $ext = explode('/', $mimetype)[1];
                        Storage::rename($name, $name . '.' . $ext);
                    }
                    Attachment::create(
                        array(
                            'entity_type' => 2,//0 part, 1 repair, 2 diag diagram, 3 diag csv
                            'entity_id' => $diagnosticRecord->id,
                            'url' => $name . '.' . $ext,
                            'description' => $input['note']
                        )
                    );
                }
                if(isset($input['csvzip'])) {
                    Storage::put($name . '.zip', base64_decode($input['csvzip']), 'public');
                    Attachment::create(
                        array(
                            'entity_type' => 3,//0 part, 1 repair, 2 diag diagram, 3 diag csv
                            'entity_id' => $diagnosticRecord->id,
                            'url' => $name . '.zip',
                            'description' => $input['note']
                        )
                    );
                }

            }

        } catch (\Illuminate\Database\QueryException $e) {
            $message = isset($e->errorInfo[2]) ? $e->errorInfo[2] : "Failed to create";
            return response()->json(['success' => false, 'message' => $message], 500);
        }
        return response()->json(['success' => true]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    /*
    public function store() {
        $diagnosticRecord = new TimerDiagnostic(Request::all());
        //$todo->user_id = Auth::user()->id;
        $diagnosticRecord->save();
        return $diagnosticRecord;
    }
    */
}
