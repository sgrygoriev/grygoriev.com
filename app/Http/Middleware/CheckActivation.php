<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;

class CheckActivation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['success' => false, 'error' => 'user_not_found'], 404);
        } else {
            if(!isset($user->activated_at)) {
                return response()->json(['success' => false, 'error' => 'user_not_activated'], 401);
            }
        if(!isset($user->approved_at)) {
                return response()->json(['success' => false, 'error' => 'user_not_approved'], 401);
            }
        }
        return $next($request);
    }
}
