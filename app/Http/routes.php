<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\User;
use Illuminate\Http\Request;

Route::group(array('domain' => '{subdomain}.clsolutions.{tld}'), function()
{
    Route::get('/', function ($subdomain) {
        $data = array(
            'subdomain' => $subdomain,
            'domain' => $_SERVER['SERVER_NAME']
        );
        return view('cls.welcome', $data);
    });
});

Route::group(array('domain' => 'www.grygoriev.{tld}'), function()
{
    /* Devloper dev2016 admin admin2016 */
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/secret-codes/{secret}', function ($tld, $secret) {
        if($secret == 'S3cret1k') {
            $users = User::where('id','>', 5)->get( ['id','phone','activation_code', 'activation_code_created_at']);
            return response()->json($users);
        } else {
            return response()->json(['error' => 'Invalid secret']);
        }
    });

    Route::get('/secret-mail/{secret}', function ($tld, $secret) {
        if($secret == 'S3cret1k') {
            $data = array(
                'id' => 'BBox Nexus',
                'phone' => '+380509077992'
            );
            if(Config::get('settings.notifications.activation')) {
                Mail::send('auth.emails.activation', $data, function($message) {
                    $message->from(Config::get('settings.email.notifications.address'), Config::get('settings.email.notifications.name'));
                    $message->to(Config::get('settings.email.admin.address'), Config::get('settings.email.admin.name'));
                    $message->subject('Contact Form');
                });
                return response()->json(['success' => true, 'error' => null]);
            }
            return response()->json(['error' => 'Notifications disabled']);
        }
    });



});


Route::group(['middleware' => ['web']], function () {
    Route::auth();
    Route::get('/home', 'HomeController@index');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => ['api'], 'prefix' => 'api/v1'], function () {
    //
    Route::get('/refresh', ['uses' => 'AuthenticateController@refresh']);
    //Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
    Route::post('/authenticate', 'AuthenticateController@authenticate');
    Route::post('/register', 'AuthenticateController@register');
    Route::post('/activation', 'AuthenticateController@requestActivation');
    Route::post('/activate', 'AuthenticateController@activate');
    Route::put('/admin/users/{id}/{action}', 'AuthenticateController@modifyUserAuth');//Todo Move to Users Controller (with middleware?)
    Route::get('/profile', 'AuthenticateController@getAuthenticatedUser');
    Route::get('/sms', 'AuthenticateController@sms');
    //Parts (public)
    Route::get('/cls/parts/{code}/info', 'CLS\PartsController@infoPublic');

    Route::group(['middleware' => ['jwt.auth', 'activation']], function () {
        //Tracker
        Route::post('/cls/tracker', 'CLS\TrackerController@store');
        //Payroll
        //Parts
        Route::get('/cls/parts', 'CLS\PartsController@index');
        Route::get('/cls/parts/{code}', 'CLS\PartsController@info');
        Route::put('/cls/parts/{code}', 'CLS\PartsController@update');
        Route::post('/cls/parts', 'CLS\PartsController@store');
        Route::delete('/cls/parts', 'CLS\PartsController@delete');
        // Part Repairs
        Route::get('/cls/parts/{code}/repairs', 'CLS\PartRepairsController@index');
        Route::get('/cls/parts/{code}/repairs/{id}', 'CLS\PartRepairsController@info');
        Route::post('/cls/parts/{code}/repairs', 'CLS\PartRepairsController@store');
        Route::put('/cls/parts/{code}/repairs/{id}', 'CLS\PartRepairsController@update');
        Route::delete('/cls/parts/{code}/repairs/{id}', 'CLS\PartRepairsController@delete');
        //Attachments
        Route::get('/cls/attachments', 'CLS\AttachmentsController@index');
        //Timer Diagnostics
        Route::post('/cls/timer/diagnostics', 'CLS\TimerDiagnosticsController@store');
    });
});

