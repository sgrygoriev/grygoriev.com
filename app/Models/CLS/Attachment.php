<?php

namespace App\CLS;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $table = 'cls_attachments';
    protected $fillable = ['entity_type', 'entity_id', 'url', 'description'];
    public $timestamps = true;
}
