<?php

namespace App\CLS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartRepair extends Model
{
    use SoftDeletes;
    //
    protected $table = 'cls_part_repairs';
    protected $fillable = ['description','part_id', 'client_id', 'repair_code', 'delivered_at', 'warranty_expires_at'];
    public $timestamps = true;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at'
    ];
}
