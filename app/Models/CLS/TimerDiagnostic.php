<?php

namespace App\CLS;

use Illuminate\Database\Eloquent\Model;

class TimerDiagnostic extends Model
{
    protected $table = 'cls_timer_diagnostics';
    protected $fillable = ['part_id', 'note'];
    public $timestamps = true;
}
