<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersWhitelist extends Model
{
    protected $table = 'users_whitelist';
    protected $fillable = [
        'phone'
    ];
    public $timestamps = false;
}
