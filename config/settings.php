<?php
/**
 * Created by PhpStorm.
 * User: serge
 * Date: 4/15/17
 * Time: 10:40 AM
 */

return [

    'notifications' => [
        'activation' => true,
        'registration' => false,
    ],

    'twitter' => [
        'url' => 'https://twitter.com/scotch_io',
        'username' => 'scotch_io'
    ],

    'email' => [
        'notifications' => [
                'address' => 'serge@grygoriev.com',
                'name' => 'Notification www.grygoriev.com'
            ],
        'admin' => [
            'address' => 'serge@grygoriev.com',
            'name' => 'Website Administrator'
        ],
    ],
    'cls' => [
        'attachments' => [
            'timerDiagnosticsFilePath' => 'public/cls/timers',
            'partsFilePath' => 'public/cls/parts',
            'repairsFilePath' => 'public/cls/repairs',
            'trackerFilePath' => 'public/cls/timers'
        ]
    ]

];
