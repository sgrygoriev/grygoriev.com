<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->index()->default(2);//0 admin 1 manager 2 user
            $table->string('phone')->nullable();
            $table->string('udid', 64)->unique();
            $table->smallInteger('os')->default(0);//0 web 1 iOS 2 Android
            $table->char('activation_code', 6)->nullable();
            $table->dateTimeTz('activation_code_created_at')->nullable();
            $table->dateTimeTz('activated_at')->nullable();
            $table->dateTimeTz('approved_at')->nullable();
            $table->string('email')->nullable();
            $table->string('name')->nullable();
            $table->string('password', 64);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
