<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClsTimerDiagnosticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cls_timer_diagnostics', function (Blueprint $t) {
            $t->engine = 'InnoDB';
            $t->increments('id')->index();
            $t->string('part_id');
            $t->string('note')->default('');
            $t->timestampsTz();//created_at & updated_at (nullable)
            $t->softDeletes();//deleted_at (nullable)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cls_timer_diagnostics');
    }
}
