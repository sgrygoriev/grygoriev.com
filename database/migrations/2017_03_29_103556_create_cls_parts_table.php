<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClsPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cls_parts', function (Blueprint $t) {
            $t->engine = 'InnoDB';
            $t->increments('id')->index();
            $t->string('code')->unique();
            $t->string('sn')->nullable();
            $t->integer('client_id')->index()->nullable();
            $t->integer('manufacturer_id')->index()->nullable();
            $t->integer('vendor_id')->index()->nullable();
            $t->string('name')->default('');//nam
            $t->string('note')->default('');//num
            $t->dateTimeTz('last_repair_code')->nullable();
            $t->dateTimeTz('last_repair_delivered_at')->nullable();
            $t->dateTimeTz('last_warranty_expires_at')->nullable();
            $t->timestampsTz();//created_at updated_at (nullable)
            $t->softDeletes();//deleted_at (nullable)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cls_parts');
    }
}
