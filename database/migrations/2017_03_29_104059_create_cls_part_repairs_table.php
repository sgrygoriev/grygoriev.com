<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClsPartRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cls_part_repairs', function (Blueprint $t) {
            $t->engine = 'InnoDB';
            $t->increments('id')->index();
            $t->string('part_id');
            $t->integer('client_id')->index()->nullable();
            $t->integer('created_by')->nullable();
            $t->integer('updated_by')->nullable();
            $t->string('repair_code')->default('');//repair code
            $t->string('description')->default('');//nam
            $t->dateTimeTz('delivered_at')->nullable();
            $t->dateTimeTz('warranty_expires_at')->nullable();
            $t->timestampsTz();//created_at updated_at (nullable)
            $t->softDeletes();//deleted_at (nullable)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cls_part_repairs');
    }
}
