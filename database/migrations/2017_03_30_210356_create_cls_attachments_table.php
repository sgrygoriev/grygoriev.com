<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClsAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cls_attachments', function (Blueprint $t) {
            $t->engine = 'InnoDB';
            $t->increments('id')->index();
            $t->integer('entity_type');
            $t->integer('entity_id')->index();
            $t->string('url')->default('');//nam
            $t->string('description')->nullable();//nam
            $t->timestampsTz();//created_at updated_at (nullable)
            $t->softDeletes();//deleted_at (nullable)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cls_attachments');
    }
}
