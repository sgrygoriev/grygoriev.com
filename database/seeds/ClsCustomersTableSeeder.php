<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\CLS\Customer;

class ClsCustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('cls_customers')->delete();

        $customers = array(
            ['name' => 'Chen Chenkie', 'email' => 'chen@gmail.com', 'password' => Hash::make('secret')],
            ['name' => 'Lee Sevilleja', 'email' => 'lee@scotch.io', 'password' => Hash::make('secret')],
        );

        // Loop through each user above and create the record for them in the database
        foreach ($customers as $customer)
        {
            Customer::create($customer);
        }

        Model::reguard();
    }
}
