<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use \App\CLS\PartRepair;

class ClsPartRepairsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('cls_part_repairs')->delete();

        $parts = array(
            ['part_id' => 1, 'repair_code' => 'D17', 'description' => 'Power Diodes Replacement'],
            ['part_id' => 1, 'repair_code' => 'PW', 'description' => 'Wires Replacement'],
            ['part_id' => 1, 'repair_code' => 'PS','description' => 'Proximity Sensor Replaced'],
            ['part_id' => 2, 'repair_code' => 'C1', 'description' => 'Faulty capacitor replaced']
        );

        // Loop through each user above and create the record for them in the database
        foreach ($parts as $part)
        {
            PartRepair::create($part);
        }

        Model::reguard();
    }
}
