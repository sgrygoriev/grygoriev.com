<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use \App\CLS\Part;
use \App\CLS\PartRepair;

class ClsPartsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('cls_parts')->delete();

        $parts = array(
            ['code' => 'Z5T40156E016K', 'name' => 'X-NUCLEO-PLC01A1'],
            ['code' => '5MCMFV', 'name' => 'Electrolitic Capacitors'],
        );

        // Loop through each user above and create the record for them in the database
        foreach ($parts as $part)
        {
            Part::create($part);
        }

        Model::reguard();
    }
}
