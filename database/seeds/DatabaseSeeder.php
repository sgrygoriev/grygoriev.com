<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(UsersWhiteListTableSeeder::class);
        $this->call(ClsPartsTableSeeder::class);
        $this->call(ClsPartRepairsTableSeeder::class);
        $this->call(ClsCustomersTableSeeder::class);
    }
}
