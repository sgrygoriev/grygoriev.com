<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users')->delete();

        $users = array(
            ['name' => 'Diagnostic',
                'group_id' => 0,
                'email' => 'intellimedia.zp@gmail.com',
                'password' => Hash::make('CnR5!8%f_r}s:mqB'),
                'phone' => '+380969829777',
                'udid' => 'DIAGNOSTICAPP2017',
                'os' => 2,// 0 web 1 ios 2 android
                'approved_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'activated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'activation_code_created_at' => Carbon::now()->format('Y-m-d H:i:s'),//date("Y-m-d H:i:s")
                'activation_code' => 777777
            ],
            ['name' => 'Serge',
                'group_id' => 0,
                'email' => 'serge@grygoriev.com',
                'udid' => 'y3jptaWncmuTyn1LQ41ZKnjjqSWZCN3s8kYYGOgAtjJ5RNLcF44RhCbp926b',
                'activated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'activation_code_created_at' => Carbon::now()->format('Y-m-d H:i:s'),//date("Y-m-d H:i:s")
                'activation_code' => 777777,
                'approved_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'password' => Hash::make('secret'),
                'phone' => '+380509077992'],
            ['name' => 'Polina',
                'group_id' => 0,
                'email' => 'polina.grigorieva@gmail.com',
                'udid' => str_random(60),
                'password' => Hash::make('secret'),
                'phone' => '+380503215190'
            ],
            ['name' => 'Pyotr',
                'group_id' => 1,
                'email' => 'intellinet.zp@gmail.com',
                'udid' => str_random(60),
                'password' => Hash::make('secret'),
                'phone' => '+13745556666'],
            ['name' => 'Adnan Kukic',
                'group_id' => 2,
                'email' => 'adnan@scotch.io',
                'phone' => '+13745556677',
                'udid' => str_random(60),
                'password' => Hash::make('secret')],
        );

        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            User::create($user);
        }

        Model::reguard();
    }
}
