<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use App\UsersWhitelist;


class UsersWhiteListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users_whitelist')->delete();

        $entries = array(
            [
                'name' => 'Timer Diagnostics',
                'phone' => '+380969829777',
            ],
            [
                'name' => 'Serge',
                'phone' => '+380509077992',
            ],

        );

        // Loop through each user above and create the record for them in the database
        foreach ($entries as $entry)
        {
            UsersWhitelist::create($entry);
        }

        Model::reguard();
    }
}
