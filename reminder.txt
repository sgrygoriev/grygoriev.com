memcached -d -p 11211

./artisan migrate:reset
./artisan migrate --seed

composer update --no-scripts
composer update

#after addition of a new model do
composer dump-autoload